using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day05 : ASolution
    {

        public Day05() : base(05, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            return Input.Split('\n').Max(GetSeatId).ToString();
        }

        protected override string SolvePartTwo()
        {
            var seatIds = Input.Split('\n').AsParallel().Select(GetSeatId).ToHashSet();
            return Enumerable.Range(0, int.MaxValue)
                .FirstOrDefault(x => !seatIds.Contains(x) && seatIds.Contains(x - 1) && seatIds.Contains(x + 1))
                .ToString();
        }

        private int GetSeatId(string line)
        {
            return 8 * ExtractPosition(line, 0,128) + ExtractPosition(line, 7, 8);
        }

        private int ExtractPosition(string line, int startPosition, int range)
        {
            var position = 0;
            for (; range > 1; startPosition++)
            {
                range /= 2;
                if (line[startPosition] == 'B' || line[startPosition] == 'R')
                {
                    position += range;
                }
            }
            return position;
        }
    }
}
