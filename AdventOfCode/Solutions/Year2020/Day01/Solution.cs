using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    class Day01 : ASolution
    {

        public Day01() : base(01, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            var nums = Input.Split('\n').Select(int.Parse).ToList();
            var num = nums.First(x => nums.Contains(2020 - x));
            return ((2020 - num) * num).ToString();
        }

        protected override string SolvePartTwo()
        {
            var nums = Input.Split('\n').Select(int.Parse).ToList();

            for(var i = 0; i < nums.Count; i++)
            {
                for(var j = i + 1; j < nums.Count; j++)
                {
                    var target = 2020 - nums[i] - nums[j];
                    if (nums.Contains(target))
                    {
                        return (target * nums[i] * nums[j]).ToString();
                    }
                }
            }
            throw new SolutionNotFoundException();
        }
    }
}

