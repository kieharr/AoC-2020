using System.Linq;
using System.Text.RegularExpressions;

namespace AdventOfCode.Solutions.Year2020
{

    class Day02 : ASolution
    {
        public Day02() : base(02, 2020, "")
        {

        }

        private readonly Regex _regex = new Regex(@"^([0-9]+)-([0-9]+)\s([a-z]):\s([a-z]+)");

        protected override string SolvePartOne()
        {
            return Input.Split('\n').Count(line =>
            {
                var splitLine = _regex.Split(line);
                var charCount = splitLine[4].Count(x => x == splitLine[3][0]);
                return charCount >= int.Parse(splitLine[1]) && charCount <= int.Parse(splitLine[2]);
            }).ToString();
        }

        protected override string SolvePartTwo()
        {
            return Input.Split('\n').Count(line =>
            {
                var splitLine = _regex.Split(line);
                return splitLine[4].ElementAtOrDefault(int.Parse(splitLine[1]) - 1) == splitLine[3][0] ^
                       splitLine[4].ElementAtOrDefault(int.Parse(splitLine[2]) - 1) == splitLine[3][0];
            }).ToString();
        }
    }
}
