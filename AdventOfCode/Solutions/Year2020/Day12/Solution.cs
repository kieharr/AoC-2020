using System;
using System.Drawing;
using System.Linq;
using MoreLinq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day12 : ASolution
    {

        public Day12() : base(12, 2020, "")
        {

        }
        
        protected override string SolvePartOne()
        {
            var ship = new Ship();
            Input.Split('\n').Select(x => new Instruction(x)).ForEach(ship.HandleInstruction);
            return ship.GetManhattan().ToString();
        }

        protected override string SolvePartTwo()
        {
            var ship = new Ship();
            Input.Split('\n').Select(x => new Instruction(x)).ForEach(ship.HandleInstructionV2);
            return ship.GetManhattan().ToString();
        }

        private enum Action
        {
            North,
            South,
            East,
            West,
            Left,
            Right,
            Forward
        }

        private readonly struct Instruction
        {
            public Instruction(string input)
            {
                Action = input.First() switch
                {
                    'N' => Action.North,
                    'S' => Action.South,
                    'E' => Action.East,
                    'W' => Action.West,
                    'L' => Action.Left,
                    'R' => Action.Right,
                    'F' => Action.Forward,
                _ => throw new Exception()
                };
                Value = int.Parse(input.Substring(1));
            }
            public Action Action { get; }
            public int Value { get; }
        }

        private class Ship
        {
            public Ship()
            {
                _direction = 90;
                _location = new Point(0, 0);
                _wayPoint = new Point(10, 1);
            }

            private int _direction;
            private Point _location;
            private Point _wayPoint;

            public int GetManhattan()
            {
                return Math.Abs(_location.X) + Math.Abs(_location.Y);
            }

            public void HandleInstruction(Instruction instruction)
            {
                switch (instruction.Action)
                {
                    case Action.North:
                        _location.Y += instruction.Value;
                        break;
                    case Action.South:
                        _location.Y -= instruction.Value;
                        break;
                    case Action.East:
                        _location.X += instruction.Value;
                        break;
                    case Action.West:
                        _location.X -= instruction.Value;
                        break;
                    case Action.Left:
                        Rotate(-instruction.Value);
                        break;
                    case Action.Right:
                        Rotate(instruction.Value);
                        break;
                    case Action.Forward:
                        MoveForward(instruction.Value);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            public void HandleInstructionV2(Instruction instruction)
            {
                switch (instruction.Action)
                {
                    case Action.North:
                        _wayPoint.Y += instruction.Value;
                        break;
                    case Action.South:
                        _wayPoint.Y -= instruction.Value;
                        break;
                    case Action.East:
                        _wayPoint.X += instruction.Value;
                        break;
                    case Action.West:
                        _wayPoint.X -= instruction.Value;
                        break;
                    case Action.Left:
                        RotateWaypoint(360 - instruction.Value);
                        break;
                    case Action.Right:
                        RotateWaypoint(instruction.Value);
                        break;
                    case Action.Forward:
                        MoveToWaypoint(instruction.Value);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            private void Rotate(int value)
            {
                _direction = (_direction + value) % 360;
                if (_direction < 0)
                {
                    _direction += 360;
                }
            }

            private void MoveForward(int value)
            {
                switch (_direction)
                {
                    case 0:
                        _location.Y += value;
                        break;
                    case 90:
                        _location.X += value;
                        break;
                    case 180:
                        _location.Y -= value;
                        break;
                    case 270:
                        _location.X -= value;
                        break;
                    default: throw new Exception(_direction.ToString());
                }
            }

            private void RotateWaypoint(int angle)
            {
                _wayPoint = angle switch
                {
                    90 => new Point(_wayPoint.Y, -_wayPoint.X),
                    180 => new Point(-_wayPoint.X, -_wayPoint.Y),
                    270 => new Point(-_wayPoint.Y, _wayPoint.X),
                    _ => _wayPoint
                };
            }

            private void MoveToWaypoint(int count)
            {
                _location.X += _wayPoint.X * count;
                _location.Y += _wayPoint.Y * count;
            }
        }
    }
}
