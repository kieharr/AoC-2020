using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day06 : ASolution
    {

        public Day06() : base(06, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            return Input.Split("\n\n").Sum(x => x.Replace("\n", string.Empty).Distinct().Count()).ToString();
        }

        protected override string SolvePartTwo()
        {
            return Input.Split("\n\n")
                .Sum(groupInput => groupInput.Replace("\n", string.Empty).Distinct()
                    .Count(groupAnswer => groupInput.Split('\n').All(x => x.Contains(groupAnswer))
                    )
                ).ToString();
        }
    }
}
