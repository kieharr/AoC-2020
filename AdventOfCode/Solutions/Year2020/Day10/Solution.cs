using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MoreLinq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day10 : ASolution
    {

        public Day10() : base(10, 2020, "")
        {
            _adapters = BuildAdapterList(Input);
        }
                
        private List<long> BuildAdapterList(string input)
        {
            var nums = input.Split('\n').Select(long.Parse).ToList();
            nums.Add(nums.Max() + 3);
            nums.Add(0);
            nums.Sort();

            return nums;
        }

        private readonly List<long> _adapters;
        private readonly Dictionary<int, long> _resultCache = new Dictionary<int, long>();
        private readonly List<int> _validJotageJumps = new List<int>{ 1, 2, 3 };

        protected override string SolvePartOne()
        {
            return GetProductOfOnesAndThrees(_adapters).ToString();
        }
        
        protected override string SolvePartTwo()
        {
            return CountCombinations(0, 0).ToString();
        }
        
        private int GetProductOfOnesAndThrees(List<long> adapters)
        {
            var ones = 0;
            var threes = 0;
            adapters.Skip(1).ForEach(adapter =>
            {
                switch (adapter - adapters[adapters.IndexOf(adapter) - 1])
                {
                    case 1:
                        ones += 1;
                        break;
                    case 3:
                        threes += 1;
                        break;
                    default:
                        throw new SolutionNotFoundException();
                }
            });
            return ones *  threes;
        }

        private long CountCombinations(long counter, int index)
        {
            if (index == _adapters.Count - 1)
            {
                _resultCache[index] = ++counter;
                return counter;
            }

            counter += _validJotageJumps.Sum(x => GetCombinationsForJoltageDifference(_adapters[index], x));

            _resultCache[index] = counter;
            return counter;
        }

        private long GetCombinationsForJoltageDifference(long adapter, int joltageDifference)
        {
            var target = adapter + joltageDifference;
            if (!_adapters.Contains(target)) return 0;
            
            var targetIndex = _adapters.IndexOf(target);
            return _resultCache.TryGetValue(targetIndex, out var result)
                ? result
                : CountCombinations(0, targetIndex);
        }
    }
}
