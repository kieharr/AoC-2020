using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{

    class Day03 : ASolution
    {
        public Day03() : base(03, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            return new TreeMap(Input).GetTreeCount(new Point(3, 1)).ToString();
        }

        protected override string SolvePartTwo()
        {
            var treeMap = new TreeMap(Input);
            return new List<Point>
            {
                new Point(1, 1),
                new Point(3, 1),
                new Point(5, 1),
                new Point(7, 1),
                new Point(1, 2)
            }
            .Select(treeMap.GetTreeCount)
            .Aggregate((res, item) => res * item)
            .ToString();
        }
        private class TreeMap
        {
            private readonly HashSet<Point> _trees;
            private readonly int _height;
            private readonly int _width;

            public TreeMap(string input)
            {
                var inputLines = input.Split('\n');
                
                _trees = new HashSet<Point>();
                _height = inputLines.Length;
                _width = inputLines[0].Length;
                
                for (var y = 0; y < _height; y++)
                {
                    for (var x = 0; x < _width; x++)
                    {
                        if (inputLines[y][x] == '#')
                        {
                            _trees.Add(new Point(x, y));
                        }
                    }
                }
            }

            public int GetTreeCount(Point slope)
            {
                var treeCount = 0;
                for (int x = 0, y = 0; y < _height; x+=slope.X, y+=slope.Y)
                {
                    if (_trees.Contains(new Point(x % _width, y)))
                    {
                        treeCount++;
                    }
                }
                return treeCount;
            }
        }
    }
}
