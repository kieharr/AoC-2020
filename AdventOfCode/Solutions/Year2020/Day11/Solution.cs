using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace AdventOfCode.Solutions.Year2020
{
    class Day11 : ASolution
    {

        public Day11() : base(11, 2020, "")
        {

        }

        protected override string SolvePartOne()
        {
            var seatMap = new SeatMap(Input);
            while (!seatMap.Stabilised)
            {
                seatMap.SetNextPositions();
            }
            return seatMap.OccupiedCount.ToString();
        }

        protected override string SolvePartTwo()
        {
            var seatMap = new SeatMap(Input, true);
            while (!seatMap.Stabilised)
            {
                seatMap.SetNextPositions();
            }
            return seatMap.OccupiedCount.ToString();
        }
        
        private class SeatMap
        {
            public bool Stabilised { get; private set; }
            
            private Dictionary<Point, SeatStatus> _map = new Dictionary<Point, SeatStatus>();
            
            private readonly bool _isV2;
            
            public SeatMap(string input, bool v2 = false)
            {
                _isV2 = v2;
                var lines = input.Split('\n');
                var width = lines.First().Length;
                
                for (var y = 0; y < lines.Length; y++)
                {
                    for (var x = 0; x < width; x++)
                    {
                        var status = lines[y][x] switch
                        {
                            'L' => SeatStatus.Empty,
                            '.' => SeatStatus.Floor,
                            '#' => SeatStatus.Occupied,
                            _ => throw new Exception()
                        };
                        _map.Add(new Point(x, y), status);
                    }
                }
            }

            public void SetNextPositions()
            {
                var changed = false;
                var newMap = _map.ToDictionary(
                    x => x.Key,
                    x =>
                    {
                        var value = _isV2 ? GetNewValueV2(x.Key) : GetNewValue(x.Key);
                        if (value != x.Value)
                        {
                            changed = true;
                        }
                        return value;
                    }
                );
                
                if (!changed)
                {
                    Stabilised = true;
                }
                else
                {
                    _map = newMap;
                }
            }

            public int OccupiedCount => _map.Count(x => x.Value == SeatStatus.Occupied);
                        
            private readonly List<Point> _directionModifiers = new List<Point>
            {
                new Point(-1, -1),
                new Point(-1, 0),
                new Point(-1, 1),
                new Point(0, -1),
                new Point(0, 1),
                new Point(1, -1),
                new Point(1, 0),
                new Point(1, 1)
            };

            private SeatStatus GetNewValue(Point point)
            {
                return _map[point] switch
                {
                    SeatStatus.Empty => GetAdjacentOccupiedCount(point) == 0? SeatStatus.Occupied: SeatStatus.Empty,
                    SeatStatus.Floor => SeatStatus.Floor,
                    SeatStatus.Occupied => GetAdjacentOccupiedCount(point) >= 4? SeatStatus.Empty: SeatStatus.Occupied,
                    _ => throw new ArgumentOutOfRangeException()
                };
            }

            private SeatStatus GetNewValueV2(Point point)
            {
                return _map[point] switch
                {
                    SeatStatus.Empty => GetVisibleOccupiedCount(point) == 0? SeatStatus.Occupied: SeatStatus.Empty,
                    SeatStatus.Floor => SeatStatus.Floor,
                    SeatStatus.Occupied => GetVisibleOccupiedCount(point) >= 5? SeatStatus.Empty: SeatStatus.Occupied,
                    _ => throw new ArgumentOutOfRangeException()
                };
            }
            
            private int GetAdjacentOccupiedCount(Point point)
            {
                var count = 0;
                for (var x = -1; x < 2; x++)
                {
                    for (var y = -1; y < 2; y++)
                    {
                        var pointToCheck = new Point(point.X + x, point.Y + y);
                        if(pointToCheck == point) continue;
                        if (!_map.TryGetValue(pointToCheck, out var location)) continue;
                        if (location == SeatStatus.Occupied)
                        {
                            count++;
                        }
                    }
                }
                return count;
            }   

            private int GetVisibleOccupiedCount(Point point)
            {
                var count = 0;

                foreach (var modifier in _directionModifiers)
                {
                    var pointToCheck = new Point(point.X + modifier.X, point.Y + modifier.Y);
                    while (_map.TryGetValue(pointToCheck, out var status))
                    {
                        if (status == SeatStatus.Empty)
                        {
                            break;
                        }
                        if (status == SeatStatus.Occupied)
                        {
                            count++;
                            break;
                        }
                        pointToCheck.X += modifier.X;
                        pointToCheck.Y += modifier.Y;
                    }
                }

                return count;
            }

            private enum SeatStatus
            {
                Empty,
                Floor,
                Occupied
            }
        }
    }
}
